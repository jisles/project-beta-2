# CarCar

Team:

* JUSTIN ISLES - SERVICES MICROSERVICE
* FRANCIS BELZA - SALES MICROSERVICE // withdrawn

## Design


## Service microservice

>>> [x] - services backend

[x] - install django app into django project

[x] - create simple model >> migrate

[x] - polling VO for automobile VIN + sold

[x] - create views + encoders
    [x] - technician views/encoders
    [x] - appointments views/encoders
    [x] - status views/encoders

[x] - URLs
    [x] - List technicians
    [x] - Create a technician
    [x] - Delete a specific technician
    [x] - List appointments
    [x] - Create an appointment
    [x] - Delete an appointment
    [x] - Set appointment status to "canceled"
    [x] - Set appointment status to "finished"

[x] - Insomnia test
    [x] - GET/POST/DELETE technicians
    [x] - GET/POST/DELETE appointments
    [x] - PUT/PUT appointments (cancel/finish)

>>> [x] - services frontend

[x] - create page.js files
    [x] - add technician
    [x] - list technicians
    [x] - add appointment
    [x] - list appointments
    [x] - success message with 3s timeout for all pages
    [x] - reset of form input fields upon submit

[x] - nav/index.js
    [x] - nav/index for services
    [x] - nav/index for inventory

[x] - special features
    [x] - VIP toggle (driven by inventory:vin status)
    [x] - service history.js


## Sales microservice

Explain your models and integration with the inventory
microservice, here.

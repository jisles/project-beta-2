import React, { useState } from "react"


function ManufacturerCreate() {

  const [name, setName] = useState('');
  const [createdManufacturer, setCreatedManufacturer] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name

    const url = 'http://localhost:8100/api/manufacturers/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      setName('');
      setCreatedManufacturer(true)
      setTimeout(() => setCreatedManufacturer(false), 3000);
    } else {
      console.log(`response error: ${JSON.stringify(response)}`)
    }
  }

  const handleSetName = (event) => {
    const value = event.target.value;
    setName(value);
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdManufacturer) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-Manufacturer-form">
                <h1 className="card-title">Add a Manufacturer</h1>
                <div className="row">
                  <div className="form-floating mb-3">
                    <input onChange={handleSetName} value={name} required placeholder="First name" type="text" id="name" name="name" className="form-control" />
                    <label htmlFor="name">Manufacturer Name</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Add Manufacturer</button>
              </form>
              <div className={messageClasses} id="success-message">
                Manufacturer added to records.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerCreate

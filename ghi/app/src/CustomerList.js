import React, { useEffect, useState } from "react"

function CustomerList() {
  const [customers, setCustomers] = useState([])

  async function loadCustomers() {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers)
    }
  }

  useEffect(() => {
    loadCustomers();
  }, []);

  async function handleDeleteCustomer(href) {
    const customerUrl = `http://localhost:8090${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const Response = await fetch(customerUrl, fetchOptions);
    if (Response.ok) {
      loadCustomers();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th></th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers && customers.map(customer => {
            return (
              <tr className="table-info" key={customer.href} value={customer.href}>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeleteCustomer(customer.href)}>
                    Delete
                  </button>
                </td>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )

}

export default CustomerList

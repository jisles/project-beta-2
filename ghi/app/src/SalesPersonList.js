import React, { useEffect, useState } from "react"

function SalesPeopleList() {
  const [salespeople, setSalesPeople] = useState([])

  async function loadSalesPeople() {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.salespeople)
    }
  }

  useEffect(() => {
    loadSalesPeople();
  }, []);

  async function handleDeleteSalesPerson(href) {
    const salespersonUrl = `http://localhost:8090${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const salespersonResponse = await fetch(salespersonUrl, fetchOptions);
    if (salespersonResponse.ok) {
      loadSalesPeople();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {salespeople?.map(salesperson => {
            return (
              <tr className="table-info" key={salesperson.employee_id} value={salesperson.employee_id}>
                <td>{salesperson.employee_id}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeleteSalesPerson(salesperson.href)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )

}

export default SalesPeopleList

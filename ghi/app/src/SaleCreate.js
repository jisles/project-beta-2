import { useState, useEffect } from "react";

function SaleCreate() {
  const [auto, setAuto] = useState('');
  const [autos, setAutos] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [salesperson, setSalesperson] = useState('');
  const [salespeople, setSalespeople] = useState([]);
  const [price, setPrice] = useState('');
  const [createdSale, setCreatedSale] = useState(false);

  const fetchAutos = async () => {
    const url = 'http://localhost:8090/api/automobileVO/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }

  const fetchCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  const fetchSalespersons = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  useEffect(() => {
    fetchCustomers();
    fetchSalespersons();
    fetchAutos();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.automobile = auto;
    data.customer = customer;
    data.salesperson = salesperson
    data.price = price
    console.log(`data is ${JSON.stringify(data)}`)
    const url = 'http://localhost:8090/api/sales/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const saleResponse = await fetch(url, fetchOptions);
    if (saleResponse.ok) {
      setAuto('');
      setCustomer('');
      setSalesperson('');
      setPrice('');
      setCreatedSale(true);
      setTimeout(() => setCreatedSale(false), 3000)
    } else {
      console.log(`saleResponse error: ${JSON.stringify(saleResponse)}`)
    }

    const moreData = {}
    moreData.sold = true
    const invUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`
    const invFetchOptions = {
      method: 'put',
      body: JSON.stringify(moreData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const invUpdateResponse = await fetch(invUrl, invFetchOptions);
    if (invUpdateResponse.ok) {
      console.log(`invUpdateResponse success.`)
    } else {
      console.log(`invUpdateResponse error: ${JSON.stringify(invUpdateResponse)}`)
    }
  }

  const handleChangeAuto = (event) => {
    const value = event.target.value;
    setAuto(value);
  }

  const handleChangeCustomer = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleChangeSalesperson = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const handleChangePrice = (event) => {
    const value = event.target.value;
    setPrice(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (salespeople.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdSale) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-sale-form">
                <h1 className="card-title">Add a sale to records</h1>
                <div className={spinnerClasses} id="loading-salesperson-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeAuto} value={auto} name="auto" id="auto" className={dropdownClasses} required>
                    <option value="">Select an automobile VIN</option>
                    {autos.map(auto => {
                      return (
                        <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeSalesperson} value={salesperson} name="salesperson" id="salesperson" className={dropdownClasses} required>
                    <option value="">Select salesperson</option>
                    {salespeople.map(salesperson => {
                      return (
                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name} / {salesperson.employee_id}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeCustomer} value={customer} name="customer" id="customer" className={dropdownClasses} required>
                    <option value="">Select customer</option>
                    {customers.map(customer => {
                      return (
                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleChangePrice} value={price} required placeholder="price of the sale" type="number" step="0.01" id="price" name="price" className="form-control" />
                    <label htmlFor="name">Price of the sale</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create Sale</button>
              </form>
              <div className={messageClasses} id="success-message">
                Sale has been added.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SaleCreate

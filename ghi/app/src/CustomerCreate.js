import React, { useState } from "react"


function CustomerCreate() {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName
    data.last_name = lastName;
    data.address = address;
    data.phone_number = phoneNumber

    const url = 'http://localhost:8090/api/customers/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const Response = await fetch(url, fetchOptions);
    if (Response.ok) {
      setFirstName('');
      setLastName('');
      setAddress('');
      setPhoneNumber('');
      setCreatedCustomer(true)
      setTimeout(() => setCreatedCustomer(false), 3000)
    } else {
      console.log(`Response error: ${JSON.stringify(Response)}`)
    }
  }

  const handleSetFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleSetLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleSetAddress = (event) => {
    const value = event.target.value;
    setAddress(value);
  }
  const handleSetPhoneNumber = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  }

  const [createdCustomer, setCreatedCustomer] = useState(false);

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdCustomer) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                <h1 className="card-title">Add a Customer</h1>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetFirstName} value={firstName} required placeholder="First name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="first_name">First Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetLastName} value={lastName} required placeholder="Last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="last_name">Last Name</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetAddress} value={address} required placeholder="Address" type="text" id="address" name="address" className="form-control" />
                      <label htmlFor="address">Address</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetPhoneNumber} value={phoneNumber} required placeholder="Phone Number" type="text" id="phone_number" name="phone_number" className="form-control" />
                      <label htmlFor="phone_number">Phone Number</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Customer added.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomerCreate

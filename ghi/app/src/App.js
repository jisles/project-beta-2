import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturerList from './ManufacturerList'
import Nav from './Nav';
import TechnicianList from './TechnicianList';
import AppointmentList from './AppointmentList';
import AppointmentCreate from './AppointmentCreate';
import TechnicianCreate from './TechnicianCreate';
import ServiceHistory from './ServiceHistory';
import ManufacturerCreate from './ManufacturerCreate';
import ModelList from './ModelList';
import ModelCreate from './ModelCreate';
import AutomobileList from './AutomobileList';
import AutomobileCreate from './AutomobileCreate';
import SalesPeopleList from './SalesPersonList';
import SalesPersonCreate from './SalesPersonCreate';
import CustomerList from './CustomerList';
import CustomerCreate from './CustomerCreate';
import SalesList from './SalesList';
import SaleCreate from './SaleCreate';
import SalePersonHistory from './SalePersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" >
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerCreate />} />
          </Route>
          <Route path="Models" >
            <Route index element={<ModelList />} />
            <Route path="new" element={<ModelCreate />} />
          </Route>
          <Route path="Automobiles" >
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileCreate />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentCreate />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="new" element={<TechnicianCreate />} />
          </Route>

          <Route path="salespeople">
            <Route index element={<SalesPeopleList />} />
            <Route path="new" element={<SalesPersonCreate />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerCreate />} />
          </Route>

          <Route path="Sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SaleCreate />} />
            <Route path="history" element={<SalePersonHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import { useState, useEffect } from "react";

function AutomobileCreate() {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [sold, setSold] = useState(false);
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);
  const [createdAutomobile, setCreatedAutomobile] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.model = model
    data.color = color;
    data.vin = vin;
    data.sold = sold;
    data.year = year;
    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const autoResponse = await fetch(autoUrl, fetchOptions);
    if (autoResponse.ok) {
      setModel('');
      setColor('');
      setVin('');
      setSold(false);
      setYear('');
      setCreatedAutomobile(true);
      setTimeout(() => setCreatedAutomobile(false), 3000)
    } else {
      console.log(`autoResponse error: ${JSON.stringify(autoResponse)}`)
    }
  }

  const handleChangeModel = (event) => {
    const value = event.target.value;
    setModel(value);
  }

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleChangeSold = (event) => {
    const value = event.target.checked;
    setSold(value);
  }

  const handleChangeYear = (event) => {
    const value = event.target.value;
    setYear(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (models.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdAutomobile) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                <h1 className="card-title">Add an automobile</h1>
                <p className="mb-3">
                  Assign a model
                </p>
                <div className={spinnerClasses} id="loading-model-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeModel} value={model} name="model" id="model" className={dropdownClasses} required>
                    <option value="">Select model</option>
                    {models.map(model => {
                      return (
                        <option key={model.id} value={model.id}>{model.name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} value={color} required placeholder="color" maxLength={17} type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeVin} value={vin} required placeholder="vin" type="vintime-local" id="vin" name="vin" className="form-control" />
                      <label htmlFor="vin">vin</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-check mb-3">
                      <input onChange={handleChangeSold} value={sold} checked={sold} placeholder="sold" type="checkbox" id="sold" name="sold" className="form-check-input" />
                      <label htmlFor="sold" className="form-check-label">sold</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeYear} value={year} required placeholder="year" type="text" id="year" name="year" className="form-control" />
                      <label htmlFor="year">year</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create Automobile</button>
              </form>
              <div className={messageClasses} id="success-message">
                Automobile has been created.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AutomobileCreate

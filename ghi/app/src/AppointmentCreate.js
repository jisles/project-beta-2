import { useState, useEffect } from "react";

function AppointmentCreate() {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date, setDate] = useState('');
  const [reason, setReason] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [createdAppointment, setCreatedAppointment] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.technician = technician
    data.vin = vin;
    data.date_time = date;
    data.reason = reason;
    data.customer = customer;
    const hatUrl = 'http://localhost:8080/api/appointments/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const apptResponse = await fetch(hatUrl, fetchOptions);
    if (apptResponse.ok) {
      setTechnician('');
      setVin('');
      setDate('');
      setReason('');
      setCustomer('');
      setCreatedAppointment(true);
      setTimeout(() => setCreatedAppointment(false), 3000)
    } else {
      console.log(`apptResponse error: ${JSON.stringify(apptResponse)}`)
    }
  }

  const handleChangeTechnician = (event) => {
    const value = event.target.value;
    setTechnician(value);
  }

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleChangeDate = (event) => {
    const value = event.target.value;
    setDate(value);
  }

  const handleChangeReason = (event) => {
    const value = event.target.value;
    setReason(value);
  }

  const handleChangeCustomer = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (technicians.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdAppointment) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">Add an appointment</h1>
                <p className="mb-3">
                  Assign a technician
                </p>
                <div className={spinnerClasses} id="loading-technician-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeTechnician} value={technician} name="technician" id="technician" className={dropdownClasses} required>
                    <option value="">Select technician</option>
                    {technicians.map(technician => {
                      return (
                        <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name} / {technician.employee_id}</option>
                      )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Appointment details
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeVin} value={vin} required placeholder="Automobile VIN" maxLength={17} type="text" id="vin" name="vin" className="form-control" />
                      <label htmlFor="name">Automobile VIN</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeDate} value={date} required placeholder="date and time" type="datetime-local" id="date" name="date" className="form-control" />
                      <label htmlFor="date">date and time</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeReason} value={reason} required placeholder="reason for the appointment" type="text" id="url" name="url" className="form-control" />
                      <label htmlFor="name">reason for the appointment</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeCustomer} value={customer} required placeholder="customer name" type="text" id="customer" name="customer" className="form-control" />
                      <label htmlFor="date">customer name</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create Appointment</button>
              </form>
              <div className={messageClasses} id="success-message">
                Appointment has been scheduled.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AppointmentCreate

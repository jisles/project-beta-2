import React, { useEffect, useState } from "react"

function ServiceHistory() {
  const [appointments, setAppointments] = useState([])
  const [searchQuery, setSearchQuery] = useState("")
  const [filteredAppointments, setFilteredAppointments] = useState([])

  async function loadAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  useEffect(() => {
    loadAppointments();
  }, [searchQuery]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    searchQuery
      ? setFilteredAppointments(appointments.filter((appointment) => appointment.vin === searchQuery))
      : setFilteredAppointments(appointments)
  }

  return (
    <div className="container my-5">
      <header>
        <h1 className="navbar-brand">Service History</h1>
        <form className="d-flex" onSubmit={handleSubmit}>
          <input
            className="form-control me-2"
            type="search"
            placeholder="Search VIN (blank search will list all)"
            aria-label="Search"
            maxLength={17}
            value={searchQuery}
            onChange={(event) => setSearchQuery(event.target.value)}
          />
          <button className="btn btn-outline-success" type="submit">Search</button>
        </form>
      </header>
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th>vin</th>
            <th>vip</th>
            <th>customer</th>
            <th>date_time</th>
            <th>technician</th>
            <th>reason</th>
            <th>status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments && filteredAppointments.map(appointment => {
            return (
              <tr className="table-info" key={appointment.href} value={appointment.href}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip ? "VIP" : "Standard"}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.employee_id}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )

}

export default ServiceHistory

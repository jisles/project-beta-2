import React, { useEffect, useState } from "react"

function SalesList() {
  const [sales, setSales] = useState([])

  async function loadSales() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  useEffect(() => {
    loadSales();
  }, []);

  async function handleDeleteSale(id) {
    const saleUrl = `http://localhost:8090/api/sales/${id}`
    console.log(`saleUrl is ${saleUrl}`)
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const saleResponse = await fetch(saleUrl, fetchOptions);
    if (saleResponse.ok) {
      loadSales();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th></th>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.map(sale => {
            return (
              <tr className="table-info" key={sale.id} value={sale.id}>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeleteSale(sale.id)}>
                    Delete
                  </button>
                </td>
                <td>{sale.salesperson.employee_id}</td>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`} </td>
                <td>{sale.automobile.vin}</td>
                <td>{`\$${sale.price}`}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )

}

export default SalesList

import React, { useEffect, useState } from "react"

function SalePersonHistory() {
  const [salesperson, setSalesperson] = useState('')
  const [salespeople, setSalespeople] = useState([])
  const [sales, setSales] = useState([])

  async function fetchSales() {
    const response = await fetch("http://localhost:8090/api/sales");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
      console.log(data.sales[0].salesperson.href)
    }
  }

  const fetchSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  useEffect(() => {
    fetchSales();
    fetchSalespeople();
  }, []);

  const handleChangeSalesperson = (event) => {
    const value = event.target.value;
    setSalesperson(value);
    console.log(salesperson)
  }


  return (
    <div className="container my-5">
      <header>
        <h1 className="navbar-brand">Sales History</h1>
        <div className="mb-3">
          <select onChange={handleChangeSalesperson} value={salesperson} name="salesperson" id="salesperson" required>
            <option value="">Select an salesperson</option>
            {salespeople.map(salesperson => {
              return (
                <option key={salesperson.href} value={salesperson.href}>{salesperson.employee_id}</option>
              )
            })}
          </select>
        </div>
      </header>
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.map(sale => {
            if (sale.salesperson.href === salesperson) {
              return (
                <tr className="table-info" key={sale.href} value={sale.href}>
                  <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{`$${sale.price}`}</td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </div>
  )

}

export default SalePersonHistory

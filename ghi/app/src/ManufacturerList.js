import React, { useEffect, useState } from "react";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([])

  async function loadManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    loadManufacturers();
  }, []);

  async function handleDeletemanufacturers(href) {
    const manufacturerUrl = `http://localhost:8100${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
    if (manufacturerResponse.ok) {
      loadManufacturers();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th>Manufacturer</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers && manufacturers.map(manufacturers => {
            return (
              <tr className="table-info" key={manufacturers.href} value={manufacturers.href}>
                <td>{manufacturers.name}</td>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeletemanufacturers(manufacturers.href)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )



}

export default ManufacturerList

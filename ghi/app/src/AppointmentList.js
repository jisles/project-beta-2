import React, { useEffect, useState } from "react"

function AppointmentList() {
  const [appointments, setAppointments] = useState([])

  async function loadAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  useEffect(() => {
    loadAppointments();
  }, []);

  async function handleDeleteAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
    if (appointmentResponse.ok) {
      loadAppointments();
    }
  }

  async function handleCancelAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}/cancel/`
    const fetchOptions = {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
    if (appointmentResponse.ok) {
      loadAppointments();
    }
  }

  async function handleFinishAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}/finish/`
    const fetchOptions = {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
    if (appointmentResponse.ok) {
      loadAppointments();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th></th>
            <th>vin</th>
            <th>vip</th>
            <th>customer</th>
            <th>date_time</th>
            <th>technician</th>
            <th>reason</th>
            <th>cancel</th>
            <th>finish</th>
          </tr>
        </thead>
        <tbody>
          {appointments && appointments.map(appointment => {
            if (appointment.status.name === "SCHEDULED") {
              return (
                <tr className="table-info" key={appointment.href} value={appointment.href}>
                  <td>
                    <button className="btn btn-sm btn-primary"
                      onClick={() => handleDeleteAppointment(appointment.href)}>
                      Delete
                    </button>
                  </td>
                  <td>{appointment.vin}</td>
                  <td>{appointment.vip ? "VIP" : "Standard"}</td>
                  <td>{appointment.customer}</td>
                  <td>{appointment.date_time}</td>
                  <td>{appointment.technician.employee_id}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    <button className="btn btn-sm btn-primary"
                      onClick={() => handleCancelAppointment(appointment.href)}>
                      Cancel
                    </button>
                  </td>
                  <td>
                    <button className="btn btn-sm btn-primary"
                      onClick={() => handleFinishAppointment(appointment.href)}>
                      Finish
                    </button>
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </div>
  )

}

export default AppointmentList

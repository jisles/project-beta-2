from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from .encoders import TechnicianEncoder, AppointmentEncoder
import json


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": f"Technician could not be created. Error: {str(e)}"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_technician(request, id):
    try:
        technician = Technician.objects.get(id=id)
        technician.delete()
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    except Technician.DoesNotExist:
        response = JsonResponse({"message": "Technician does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content["vin"]
            in_inventory = AutomobileVO.objects.filter(vin=vin)
            print(f"in_inventory is: {in_inventory}")
            if in_inventory:
                content["vip"] = True
            else:
                content["vip"] = False
            tech_id = content["technician"]
            tech = get_object_or_404(Technician, id=tech_id)
            content["technician"] = tech
            appointment = Appointment.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": f"Could not create the appointment. Error: {str(e)}"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Could not modify the appointment"})
        response.status_code = 400
        return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Could not modify the appointment"})
        response.status_code = 400
        return response

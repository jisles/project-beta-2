from common.json import ModelEncoder
from .models import Technician, Status, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class StatusEncoder(ModelEncoder):
    model = Status
    properties = [
        "name",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "vip",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "status": StatusEncoder(),
    }

from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Salesperson, Sale, Customer, AutomobileVO
from .encoders import (
    CustomerDetailEncoder,
    CustomerEncoder,
    SalespersonEncoder,
    AutomobileVOEncoder,
    )
import json


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "salesperson",
        "customer",
        "automobile",
        "price",
    ]
    encoders = {
        "customer": CustomerDetailEncoder(),
        "salesperson": SalespersonEncoder(),
        "automobile": AutomobileVOEncoder(),
    }


@require_http_methods(["GET"])
def api_AutomobileVOs(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVOEncoder
        )


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )

    else:
        content = json.loads(request.body)
        salesperson_id = content["salesperson"]
        salesperson = Salesperson.objects.get(id=salesperson_id)
        content["salesperson"] = salesperson
        customer_id = content["customer"]
        customer = Customer.objects.get(id=customer_id)
        content["customer"] = customer
        vin = content["automobile"]
        autovo = AutomobileVO.objects.get(vin=vin)
        content["automobile"] = autovo
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    try:
        sale = Sale.objects.get(pk=id)
        sale.delete()
        return JsonResponse({"message": "Sale was deleted."})
    except Exception as e:
        response = JsonResponse(
            {"error": f"Sale was not deleted. Error: {str(e)}"}
        )
        response.status_code = 400
        return response


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
        customer.delete()
        return JsonResponse({"message": "Customer has been deleted."})
    except Customer.DoesNotExist:
        return JsonResponse({"error": "Customer not found."}, status=404)


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse({"salespeople": salespeople}, encoder=SalespersonEncoder)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    try:
        salesperson = Salesperson.objects.get(pk=id)
        salesperson.delete()
        return JsonResponse({"message": "Salesperson has been deleted."})
    except Salesperson.DoesNotExist:
        return JsonResponse({"error": "Salesperson not found."}, status=404)
